<?php 
require_once("include/header.php");
require_once("database/service.php");

$jmlK = $jmlKriteria[0]['jmlKriteria'];
$jmlA = $jmlAlternatif[0]['jmlAlternatif'];

   $kriteria = $_POST['kriteria'];
   $mix = $_POST['mix'];

   // ----------------------------------- LANGKAH-LANGKAH ------------------------------------------------//
   // Langkah 1 dan 2
   // ubah Array inputan (index bukan angka) ->> Matrix (array dengan index angka) untuk Kriteria ($kriteria) dan Alternatif ($mix)
   $mKriteria = buildKriteriaMatrix($kriteria);
   $mAlter = buildAlternatifMatrix($mix);
   $matriksAlter = $mAlter;
   // echo "Kriteria";
   // pre($mKriteria);
   //Langkah 3
   /* Menghitung Eign Vector matrix Kriteria dengan cara :
      A.    Kuadrat Matrix Kriteria
      B.    Menjumlahkan nilai tiap kolom di matriks baru dari langkah A
      C.    Mencari total nilai dari matriks di langkah B
      D.    Membagi tiap nilai di matrix di langkah B dengan total nilai di langkah C shg terbentuk matrix Eign Vector untuk Kriteria 
   */
   $mKrit = kuadratMatrix($mKriteria);
   $sumColK = sumColKriteria($mKrit); //matrix baru dengan 1 kolom
   $sumColRowK = sumColRowKriteria($sumColK);
   $evKriteria = buildEignVectorKriteria($sumColK, $sumColRowK);
   // echo "<br>Eign Vector Kriteria";
   // pre($sumColK);

   //Langkah 4
   /*  Menghitung Eign Vector matrix Alternatif dengan cara :
      A.    Kuadrat Matrix Alternatif
      B.    Menjumlahkan nilai tiap kolom di matriks baru dari langkah A
      C.    Mencari total nilai dari matriks di langkah B
      D.    Membagi tiap nilai di matrix di langkah B dengan total nilai di langkah C shg terbentuk matrix Eign Vector untuk Alternatif 
   */
   $mAlter = getAlterMatrix($mAlter);
   $sumColA = sumColAlternatif($mAlter);
   $sumColRowA = sumColRowAlternatif($sumColA);
   $evAlter = buildEignVectorAlternatif($sumColA, $sumColRowA);
   // echo "<br>Eign Vector Alternatif";
   // pre($sumColA);

   //Langkah 5
   // Menghitung Prioritas Kriteria dengan cara : evAlter * evKriteria
   $prioritasAlternatif = multiplicationEignVector($evAlter, $evKriteria);
   // pre($prioritasAlternatif);
   $terpilih = selectedAlternatif($prioritasAlternatif); //alternatif terpilih
   $history = getAlterNameWithStatus($alternatif, $prioritasAlternatif, $terpilih); //alternatif terpilih
   $record = $jmlRecordHistory;
   // insert ke database di tbl history dan tbl_alternatif_proses
   $finalProcess = historyTambah($history, $record);
   // pre($history);
   // // --------------------------------------- SELESAI ----------------------------------------------------//

   /* 
      UJI KONSISTENSI
      Langkah 1
      Menghitung VJT dengan cara : Pairwise Comparation Kriteria * Prioritas Kriteria
      $vjt = multiplicationVJT($kriteria, $prioritasKriteria);
      pre($vjt);
      Langkah 2
      Menghitung Vector Konsistensi dengan cara : VJT / prioritas kriteria 
      $vctKonsistensi = divisionVector($vjt, $prioritasKriteria);
      Langkah 3
      Menghitung Lambda dan Indeks Konsistensi dengan cara :
      1.  Lambda              : mencari rata-rata Vector Konsistensi
      2.  Indeks Konsistensi : (Lambda-n) / n-1 
   */

   //kumpulan function
   function selectedAlternatif($kandidat){
      $max = $temp = 0;
      $person = 0;
      //This loop is to get max value from array
      for ($i = 0 ; $i < count($kandidat); $i++) {
         if ($i == 0) {
            $max = $temp = $kandidat[$i];
            $person = $i;
         }
         if ($i > 0) {
            if ($kandidat[$i] > $temp) {
                  $max = $kandidat[$i];
                  $person = $i;
            }
         }
      }
      return $person;
   }

   function getAlterNameWithStatus($altr, $prio, $sltd){
      // pre($altr[$sltd]);
      $matrix = [];
      $status = ['Terpilih', 'Tidak Terpilih'];
      // echo $sltd;
      $selected = $sltd;
      for ($i=0; $i < count($altr); $i++){
         // array_push($matrix, $value);
         // pre($value);
         array_push($matrix, $altr[$i]);
         array_push($matrix[$i], $prio[$i]);
         if($i != $sltd){
            array_push($matrix[$i], $status[1]);
         }else{
            array_push($matrix[$i], $status[0]);
         }
      }
      return $matrix;
   }

   function divisionVector($a, $priority){
      $matrix = [];
      for ($i=0; $i < sizeof($a); $i++) { 
         $hasil = 0;
         $hasil = $a[$i] / $priority[$i];
         array_push($matrix, $hasil);
      }
      echo "vector konsistensi";
      // pre($matrix);

      return $matrix;
   }

   function transposeMatrix($data){
      $retData = [];
      foreach ($data as $row => $columns) {
         foreach ($columns as $row2 => $column2) {
            $retData[$row2][$row] = $column2;
         }
      }
      return $retData;
   }

   function multiplicationVJT($pairwise, $priority){
      // echo "<br><br><br><br><br><br>";
      // pre($pairwise);
      // echo "<br><br><br><br><br><br>";
      // pre($priority);
      $matrix = [];
      foreach ($pairwise as $key => $value) {
         $hasil = 0;
         for ($v=0; $v < sizeof($value); $v++) { 
            $hasil += $value[$v] * $priority[$v];
         }
         array_push($matrix, $hasil);
      }

      // echo "<br><br><br><br><br><br>";
      // echo "VJT";
      // pre($matrix);
      return $matrix;
   }

   function multiplicationEignVector($alter, $krit){
      $matrix = [];
      
      $tpAlter = transposeMatrix($alter);
      $kolom = sizeof($tpAlter);
      $baris = sizeof($tpAlter[0]);

      // echo $kolom;
      // echo $baris;

      // echo "<br>Transpose Alternatif";
      // pre($tpAlter);
      // pre($krit);
      
      for ($column=0; $column < $kolom; $column++) { 
         $hasil = 0;
         for ($row=0; $row < $baris; $row++) { 
            $hasil += $tpAlter[$column][$row] * $krit[$row];
         }
         array_push($matrix, round($hasil, 2));
      }
      return $matrix;
   }

   function kuadratMatrix($mtx){
      $matrix = [];
      $matrix1 = $mtx;

      $matrix2 = $mtx;
      for ($i=0; $i<sizeof($matrix1); $i++) {
         for ($j=0; $j<sizeof($matrix2[$i]); $j++) {
            $temp = 0;
            for ($k=0; $k<sizeof($matrix2[$j]); $k++) {
                  $temp += $matrix1[$i][$k] * $matrix2[$k][$j];
            }
            $matrix[$i][$j] = round($temp, 2);
         }
      }
      
      return $matrix;
   }

   function getAlterMatrix($mtx){
      $matrix = [];
      for ($a=0; $a < sizeof($mtx); $a++) { 
         $temp = kuadratMatrix($mtx[$a]);
         array_push($matrix, $temp);
      }
      return $matrix;
   }

   function sumColAlternatif($mtx){
      $hasil = 0;
      $matrix = [];
      foreach ($mtx as $key => $value) {
         $matrix1 = [];
         foreach ($value as $key => $value1) {
            foreach ($value1 as $key => $nilai) {
                  $hasil += $nilai;
            }
            array_push($matrix1, $hasil);
            $hasil = 0;
         }
         array_push($matrix, $matrix1);
      }
      return $matrix;
   }

   function sumColRowAlternatif($mtx){
      $hasil = 0;
      $matrix = [];
      for ($i=0; $i < sizeof($mtx); $i++) { 
         for ($k=0; $k < sizeof($mtx[$i]); $k++) { 
            $hasil += $mtx[$i][$k];
         }
         array_push($matrix, $hasil);
         $hasil = 0;
      }
      return $matrix;
   }

   function buildEignVectorAlternatif($mtx, $divider){
      $matrix = [];
      $hasil = 0;
      for ($m=0; $m < sizeof($mtx); $m++) { 
         $matrix1 = [];
         for ($i=0; $i < sizeof($mtx[$m]); $i++) { 
            $hasil = $mtx[$m][$i]/$divider[$m];
            array_push($matrix1, round($hasil, 2));
         }
         array_push($matrix, $matrix1);
      }

      return $matrix;
   }

   function sumColKriteria($mtx){
      $hasil = 0;
      $matrix = [];
      for ($b=0; $b < sizeof($mtx); $b++) { 
         for ($k=0; $k < sizeof($mtx[$b]); $k++) { 
            $hasil += $mtx[$k][$b];
         }
         array_push($matrix, $hasil);
         $hasil = 0;
      }
      return $matrix;
   }

   function sumColRowKriteria($mtx){
      $total = 0;
      for ($c=0; $c < sizeof($mtx); $c++) { 
         $total += $mtx[$c];
      }
      return round($total, 2);
   }

   function buildEignVectorKriteria($mtx, $divider){
      $matrix = [];
      $hasil = 0;
      for ($m=0; $m < sizeof($mtx); $m++) { 
         $hasil = $mtx[$m]/$divider;
         array_push($matrix, round($hasil, 2));
      }

      return $matrix;
   }

   function buildKriteriaMatrix($kriteriaArr){
      $matrix = [];
      foreach ($kriteriaArr as $index => $value) {
         $baris = [];
         for ($i=0; $i < count($value); $i++) {
            array_push($baris, $value[$i]);
         }
         array_push($matrix, $baris);
      }
      return shuffleKriteriaMatrixByKolom($matrix);
   }

   function shuffleKriteriaMatrixByKolom($mtx){
      $matrix = [];
      for ($b=0; $b < count($mtx); $b++) { 
         $baris = [];
         for ($k=0; $k < count($mtx[$b]); $k++) { 
            array_push($baris, $mtx[$k][$b]);
         }
         array_push($matrix, $baris);
      }
      return $matrix;
   }

   function buildAlternatifMatrix($mixArr){
      $matrix = [];
      foreach ($mixArr as $index => $indexKriteria) {
         $baris = [];
         foreach ($indexKriteria as $key => $indexAlter) {
            $kolom = [];
            for ($x=0; $x < sizeof($indexAlter); $x++) { 
                  array_push($kolom, $indexAlter[$x]);
            }
            array_push($baris, $kolom);
         }
         array_push($matrix, $baris);
      }
      return shuffleAlternatifMatrixByKolom($matrix);
   }

   function shuffleAlternatifMatrixByKolom($mtx){
      $matrix = [];
      for ($b=0; $b < count($mtx); $b++) { 
         $baris = [];
         for ($k=0; $k < count($mtx[$b]); $k++) { 
            $kolom = [];
            for ($v=0; $v < count($mtx[$b][$k]); $v++) { 
                  array_push($kolom, $mtx[$b][$k][$v]);
            }
            array_push($baris, $kolom);
         }
         array_push($matrix, $baris);
      }
      return $matrix;
   }

   function pre($array){
      echo "<pre>";
      print_r($array);
      echo "</pre>";
   }

   function showPerhitunganKuadratMatriks($arr){
      // pre($arr);
      for ($i=0; $i < sizeof($arr); $i++){
         echo "<tr>";
         for ($x=0; $x < sizeof($arr[$i]); $x++){
            echo "<td>".$arr[$x][$i]."</td>";
         }
         if($i==0){
            echo "<td rowspan=".sizeof($arr)."><br><br><br><center><b>X</center></td>";
         }
         for ($x=0; $x < sizeof($arr[$i]); $x++){
            echo "<td>".$arr[$x][$i]."</td>";
         }
         echo "</tr>";
      }
   }

   function showPerhitunganKuadratMatriksAlter($arr){
      for ($i=0; $i < sizeof($arr); $i++){
         echo "<tr>";
         for ($x=0; $x < sizeof($arr[$i]); $x++){
            echo "<td>".$arr[$i][$x]."</td>";
         }
         if($i==0){
            echo "<td rowspan=".sizeof($arr)."><br><br><br><center><b>X</center></td>";
         }
         for ($x=0; $x < sizeof($arr[$i]); $x++){
            echo "<td>".$arr[$i][$x]."</td>";
         }
         echo "</tr>";
      }
   }

   function showHasilKuadratTable($arr){
      for ($i=0; $i < sizeof($arr); $i++){
         echo "<tr>";
         for ($x=0; $x < sizeof($arr[$i]); $x++){
            echo "<td>".$arr[$x][$i]."</td>";
         }
         echo "</tr>";
      }
   }

   function showSingleColumnTable($arr){
      
      for ($x=0; $x < sizeof($arr); $x++){
         echo "<tr>";
         echo "<td>".$arr[$x]."</td>";
         echo "</tr>";
      }
      
   }

   function showPrioritasAlternatif($arr){
      for ($x=0; $x < sizeof($arr); $x++){
         echo "<tr>";
         echo "<td>".$arr[$x]."</td>";
         echo "</tr>";
      }
   }

   function showAlternatifMatrix($arr, $row){?>
      <div class="panel panel-info">
         <div class="panel-heading">
            <div class="form-group">
               <h4>Matriks Alternatif</h4>
               <h5>2. Hasil Kuadrat Matriks Alternatif terhadap Kriteria-<?= $row?></h5>
            </div>
         </div>
         <div class="panel-body">
            <div class="table-responsive">
               <table class="table table-bordered">
                  <tbody>
                  <?php showHasilKuadratTable($arr);?>
               </table>
            </div>
         </div>
      </div>
   <?php 
   }  
?>

<div class="content-wrapper">
   <section class="content-header">
      <h1>Olah Data</h1>
	   <ol class="breadcrumb">
         <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Olah Data</li>
      </ol>
   </section>
   <section class="content">
   <!-- Matriks Kriteria -->
   <div class="panel panel-info">
      <div class="panel-heading">
         <div class="form-group">
            <h4>Matriks Kriteria</h4>
            <h5>1. Kuadrat Matriks</h5>
         </div>
      </div>
      <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-bordered">
               <tbody>
               <?php showPerhitunganKuadratMatriks($mKriteria);?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <div class="panel panel-info">
      <div class="panel-heading">
         <div class="form-group">
            <h4>Matriks Kriteria</h4>
            <h5>2. Hasil Kuadrat Matriks</h5>
         </div>
      </div>
      <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-bordered">
               <tbody>
               <?php showHasilKuadratTable($mKrit);?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <div class="panel panel-info">
      <div class="panel-heading">
         <div class="form-group">
            <h4>Matriks Kriteria</h4>
            <h5>3. Hasil penjumlahan nilai disetiap baris pada matriks Kriteria</h5>
         </div>
      </div>
      <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-bordered">
               <tbody>
               <?php showSingleColumnTable($sumColK);?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <div class="panel panel-danger">
      <div class="panel-heading">
         <div class="form-group">
            <h4>Matriks Kriteria</h4>
            <h5>4. Eign Vector</h5>
         </div>
      </div>
      <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-bordered">
               <tbody>
               <?php showSingleColumnTable($evKriteria);?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
   <!-- LANGKAH ALTERNATIF
         Matriks Alternatif untuk Kriteria - Alternatif 3 
   -->
   <?php $row = 0;
      foreach ($matriksAlter as $key => $value) {
      $row++;
      ?>
      <div class="panel panel-info">
         <div class="panel-heading">
            <div class="form-group">
               <h4>Matriks Alternatif</h4>
               <h5>1. Kuadrat Matriks Alternatif terhadap Kriteria-<?= $row?></h5>
            </div>
         </div>
         <div class="panel-body">
            <div class="table-responsive">
               <table class="table table-bordered">
                  <tbody>
                  <?php showPerhitunganKuadratMatriksAlter($value);?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <?php }
      $row = 0;
      foreach ($mAlter as $key => $value) {
         $row++;
         showAlternatifMatrix($value, $row);
      }?>
      <div class="panel panel-info">
         <div class="panel-heading">
            <div class="form-group">
               <h4>Matriks Alternatif</h4>
               <h5>3. Hasil penjumlahan nilai disetiap baris pada matriks Alternatif</h5>
            </div>
         </div>
         <div class="panel-body">
            <div class="table-responsive">
               <table class="table table-bordered">
                  <tbody>
                     <?php showSingleColumnTable($sumColRowA);?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <?php 
      $row = 0;
      foreach ($evAlter as $key => $value):
      $row++;
      ?>
      <div class="panel panel-danger">
         <div class="panel-heading">
            <div class="form-group">
               <h4>Matriks Alernatif</h4>
               <h5>4. Eign Vector Alternatif terhadap Kriteria-<?=$row;?></h5>
            </div>
         </div>
         <div class="panel-body">
            <div class="table-responsive">
               <table class="table table-bordered">
                  <tbody>
                     <?php showSingleColumnTable($value);?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <?php endforeach?>
      <div class="panel panel-success">
         <div class="panel-heading">
            <div class="form-group">
               <h4>Prioritas Alternatif</h4>
               <h5>Prioritas Alternatif sesuai dengan Kriteria yang tersedia</h5>
            </div>
         </div>
         <div class="panel-body">
            <div class="table-responsive">
               <table class="table table-bordered">
                  <tbody>
                     <?php showPrioritasAlternatif($prioritasAlternatif);?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      </div>
   </section>
</div>

<?php
require_once 'include/footer.php';
?>