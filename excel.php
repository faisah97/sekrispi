<?php 
require_once('database/service.php');
require_once('plugins/composer/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

$data = $dataHistory; //data from db

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1','No.');
$sheet->setCellValue('B1','Kode');
$sheet->setCellValue('C1','NIP');
$sheet->setCellValue('D1','Nama');
$sheet->setCellValue('E1','Email');
$sheet->setCellValue('F1','Telepon');
for ($index=0; $index < count($data); $index++) { 
   $cell = $index+2;
   $sheet->setCellValue('A'.$cell,$data[$index]['id']);
   $sheet->setCellValue('B'.$cell,$data[$index]['kode_alt']);
   $sheet->setCellValue('C'.$cell,$data[$index]['nip_alt']);
   $sheet->setCellValue('D'.$cell,$data[$index]['nama_alt']);
   $sheet->setCellValue('E'.$cell,$data[$index]['email_alt']);
   $sheet->setCellValue('F'.$cell,$data[$index]['telp_alt']);
}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Histori Kepemimpinan.xlsx"');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
?>