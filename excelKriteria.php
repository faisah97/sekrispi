<?php 
require_once('database/service.php');
require_once('plugins/composer/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

$data = $kriteria; //data from db

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1','No.');
$sheet->setCellValue('B1','Kode Kriteria');
$sheet->setCellValue('C1','Nama Kriteria');

for ($index=0; $index < count($data); $index++) {
    $cell = $index+2;
    $sheet->setCellValue('A'.$cell,$index+1);
    $sheet->setCellValue('B'.$cell,$data[$index]['kode_kriteria']);
    $sheet->setCellValue('C'.$cell,$data[$index]['nama_kriteria']);
}

// echo "<pre>";
// print_r($data);
// echo "</pre>";

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Daftar Kriteria.xlsx"');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
?>