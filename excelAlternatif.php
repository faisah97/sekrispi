<?php 
require_once('database/service.php');
require_once('plugins/composer/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

$data = $alternatif; //data from db

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1','No.');
$sheet->setCellValue('B1','Kode Alternatif');
$sheet->setCellValue('C1','NIP');
$sheet->setCellValue('D1','Nama Alternatif');
$sheet->setCellValue('E1','Email Alternatif');
$sheet->setCellValue('F1','Telepon Alternatif');

for ($index=0; $index < count($data); $index++) {
    $cell = $index+2;
    $sheet->setCellValue('A'.$cell,$index+1);
    $sheet->setCellValue('B'.$cell,$data[$index]['kode_alternatif']);
    $sheet->setCellValue('C'.$cell,$data[$index]['nip_alternatif']);
    $sheet->setCellValue('D'.$cell,$data[$index]['nama_alternatif']);
    $sheet->setCellValue('E'.$cell,$data[$index]['email_alternatif']);
    $sheet->setCellValue('F'.$cell,$data[$index]['telepon_alternatif']);
}

// echo "<pre>";
// print_r($data);
// echo "</pre>";

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Daftar Alternatif.xlsx"');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
?>