<?php 
require_once('database/service.php');
require_once('plugins/composer/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

if(isset($_GET['id'])){
    $id_history = $_GET['id'];
    $detailHistory = tampilDetailHistory($id_history); //data from db
    // pre($detailHistory);
    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1','No.');
    $sheet->setCellValue('B1','Kode');
    $sheet->setCellValue('C1','NIP');
    $sheet->setCellValue('D1','Nama');
    $sheet->setCellValue('E1','Email');
    $sheet->setCellValue('F1','Telepon');
    $sheet->setCellValue('G1','Nilai');
    $sheet->setCellValue('H1','Status');
    $cell = 1;
    $nomor = 1;
    foreach ($detailHistory as $key => $value) {
        $cell++;
        $sheet->setCellValue("A$cell", $nomor++);
        $sheet->setCellValue("B$cell", $value['kode']);
        $sheet->setCellValue("C$cell", $value['nip']);
        $sheet->setCellValue("D$cell", $value['nama']);
        $sheet->setCellValue("E$cell", $value['email']);
        $sheet->setCellValue("F$cell", $value['telepon']);
        $sheet->setCellValue("G$cell", $value['nilai']);
        $sheet->setCellValue("H$cell", $value['status']);
    }

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Riwayat Kepemimpinan '.$id_history.'.xlsx"');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
}
?>