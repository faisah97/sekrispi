<?php 
  require_once("include/header.php");
  require_once("database/service.php");

  // pre($dataHistory);
  if(isset($_GET['history'])){
    $id_history = $_GET['history'];
    $detailHistory = tampilDetailHistory($id_history);
?>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Riwayat Kepemimpinan <?=$id_history ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Hasil</li>
      </ol>
    </section>
    <section class="content">
<div class="panel panel-default">
  <div class="panel-heading">
    <form action="excelDetailHistory.php?id=<?= $id_history?>" method="post" class="form-inline">
      <div class="form-group">
        <button type="submit" class="btn btn-info">
          <span class="glyphicon glyphicon-print"></span> Download Excel File
        </button>
        <!-- <a class="btn btn-default" target="_blank" href=""><span class="glyphicon glyphicon-print"></span> Download Excel File</a> -->
      </div>
    </form>
  </div>
  <div class="table-responsive">
    <table class="table table-bordered table-hover table-striped">
    <thead>
      <tr>
        <th>No.</th>
        <th>Kode</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Telepon</th>
        <th>Nilai</th>
        <th>Status</th>
      </tr>
      <?php
       $nomor = 1;
       foreach ($detailHistory as $key => $value):?>
        <tr>
          <td><?= $nomor++?></td>
          <td><?= $value['kode']?></td>
          <td><?= $value['nip']?></td>
          <td><?= $value['nama']?></td>
          <td><?= $value['email']?></td>
          <td><?= $value['telepon']?></td>
          <td><?= $value['nilai']?></td>
          <td><?= $value['status']?></td>
        </tr>
        <?php endforeach?>
    </thead>  
    </table>
  </div>
</div>
    </section>

<?php 
}
require_once("include/footer.php"); ?>
  